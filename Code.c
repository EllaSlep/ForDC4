#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <iso646.h>
typedef struct node{//односвязный список
    char* value;
    struct node* next;
}node;


void push(node** head, char* data){
    node* tmp = malloc(sizeof(node));
    tmp->value = calloc(strlen(data) + 1, sizeof(char));
    strcpy(tmp->value, data);
    tmp->next = *head;
    (*head) = tmp;
}

char* pop(node** head){
    if (*head == NULL){
        printf("Error: list is empty");
        exit(-1);
    }
    char* res = (**head).value;
    *head = (*head)->next;
    return res;
}


const int maxReqLen = 2048;
const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

typedef struct data {
    char *remoteAddr;
    char *localTime;
    char *timeZone;
    char *request;
    char *status;
    char *bytesSend;
} data;

data getData(char *request) {//эта функция разделяет нашу строку на части
    data res;

    res.remoteAddr = strtok(request, "-");

    strtok(NULL, "[");
    res.localTime = strtok(NULL, " ");

    res.timeZone = strtok(NULL, "]");

    strtok(NULL, "\"");
    res.request = strtok(NULL, "\"");

    res.status = strtok(NULL, " ");
    res.bytesSend = strtok(NULL, "\n");

    return res;

}

int getMonthNumber(char *mon) {
    int res;
    for (res = 0; res < 12 and strcmp(mon, months[res]); res++);
    return res + 1;
}

//время Unix-измеряется количеством секунд, прошедших с «эпохи» (начало 1970 года по UTC)
int getTime(char *reqTime) {
    int res;
    struct tm localTime = {};

    localTime.tm_mday = atoi(strtok(reqTime, "/"));//считываем пока не дойдём до "/"
    localTime.tm_mon = getMonthNumber(strtok(NULL, "/"));//мы продолжаем поиск в оригинальной строке->ставим NULL
    localTime.tm_year = atoi(strtok(NULL, ":")) - 1900; // years since 1900
    localTime.tm_hour = atoi(strtok(NULL, ":"));
    localTime.tm_min = atoi(strtok(NULL, ":"));
    localTime.tm_sec = atoi(strtok(NULL, ":"));

    res = mktime(&localTime);//конвертирует время в календарное время(1 число на выход)

    return res;
}

//new function
void timeWindowSearch(long long **res, long long ReqCnt, int tmWindow, int *tmReq) {//поиск временного окна размером в tmWindow
    //res массив [0] - левая граница, [1] - правая граница, [2] - количество запросов
    //ReqCnt- номер строки
    //tmWindow -временное окно
    //tmReq -массив со временем запроса
    long long mxLen = 0, j = 1;

    for (long long i = 0; i < ReqCnt; i++) {//перебор всех времён
        while (j < ReqCnt and tmReq[j] - tmReq[i] <= tmWindow) {//число на правой - число на левой укладывается в наше временное окно мы увеличиваем правую границу
            j++;
        }
        if (tmReq[j] - tmReq[i] > tmWindow) {//смотрим максимальная ли длина
            if (j - i > mxLen) {//ищем макс.временое окно
                mxLen = j - i;
                (*res)[0] = tmReq[i];//начало временного окна
                (*res)[1] = tmReq[j - 1];//конец временного окна
                (*res)[2] = mxLen;//его длина
            }
        } else {
            if (j - i > mxLen) {
                mxLen = j - i;
                (*res)[0] = tmReq[i];
                (*res)[1] = tmReq[j - 1];
                (*res)[2] = mxLen;
            }
            break;
        }
    }

}

int dataCorrect(data curData) {//проверка, что запрос вообще верен
    return !(curData.localTime == NULL or curData.request == NULL or curData.status == NULL or
             curData.timeZone == NULL or curData.remoteAddr == NULL or curData.bytesSend == NULL);
}

int main(int argc, char *argv[]) {
    node *requests = NULL;//создали очередь
    
    FILE *logFile = fopen("/Users/selihovkinaekaterina/Desktop/forC/NASA_access_log_Jul95", "r");
    
    int timeWindow = 2;//размер временного окна(2й аргумент)(когда количество запросов на сервер было максимально)
    long long linesLimit = 128; // Будем динамически выделять память, увеличивая её в два раза при переполнении
    long long stringCnt = 0;
    long long ErrCnt = 0;//сколько у нас ошибок 5**
    char *curReq = calloc(maxReqLen, sizeof(char));//массив
    int *requestTimes = calloc(linesLimit, sizeof(long long));//массив куда записываем время в секундах каждого запроса

    while (!feof(logFile)) {//пока не конец файла

        fgets(curReq, maxReqLen, logFile);
        data curReqData = getData(curReq);//мы разделили наш запрос на части и теперь это тип data

        if (dataCorrect(curReqData)) {//если запрос вообще верен
            if (stringCnt == linesLimit) {
                linesLimit *= 2;
                requestTimes = realloc(requestTimes, linesLimit * sizeof(long long));
            }

            requestTimes[stringCnt] = getTime(curReqData.localTime);//заносим в массив время запроса

            if (curReqData.status[0] == '5') {//статус начинается с 5(серверные ошибки)(500-внутр.ошибка, 501-Метод запроса не поддерживается сервером и не может быть обработан...503-cервер недоступен)
                push(&requests,curReqData.request); // помещаем искомый запрос в очередь.
                ErrCnt++;
            }
            stringCnt++;
        }
    }

    long long *tmWindowInterval = calloc(3,sizeof(long long)); // [0] - левая граница, [1] - правая граница, [2] - количество запросов.
    timeWindowSearch(&tmWindowInterval, stringCnt, timeWindow, requestTimes);

    printf("Длина временного окна: %d\n", timeWindow);
    printf("Максимальное количество запросов(%lld) от %lld до %lld\n\n", tmWindowInterval[2], tmWindowInterval[0], tmWindowInterval[1]);

    printf("В файле  %lld 5xx errors\n\nСписок запросов:\n", ErrCnt);
    
    for (long long i = 0; i < ErrCnt; i++)//печатаем все ошибки 5**
        printf("%s\n", pop(&requests));

    return 0;
}